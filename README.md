# OpenStack Deployment v2

It's an ansible playbook created for setting up [OpenStack](https://www.openstack.org/). It uses DevStack which is a series of scripts used to bring up a complete OpenStack environment. 
It takes about 30 minutes to bring up controller and less than 20 minutes to bring up a compute node.

---

## Prerequisites

To run this playbook you will need [Ansible](https://www.ansible.com/) which is simple, agentless automation tool.

---

## Usage

To use this playbook edit following inventory files, which are located in `inventories/lab/`:
  - `hosts.yaml` - add or remove hosts and change their IP addresses
	- `group_varsall.yaml` - add passwords and change the IP network

After changing these files you can run the playbook using the following command:

```sh
ansible-playbook deployment.yaml
```

---

